const srcListen = document.querySelector('#gt-src-listen')
const srcClear = document.querySelector('#gt-clear')
const destCopy = document.querySelector('#gt-res-copy')
const destListen = document.querySelector('#gt-res-listen')
const langSwap = document.querySelector('#gt-swap')
const srcArea = document.querySelector('#gt-src-wrap')
const source = document.querySelector('#source')

const phraseBook = document.querySelector('#gt-pb-star').children[0]

const srcLangMenu = document.querySelector('#gt-sl-gms').children[1]
const destLangMenu = document.querySelector('#gt-tl-gms').children[1]

const langContainer = document.querySelector('.sl-sugg-button-container')
const lang1 = langContainer.children[0]
const lang2 = langContainer.children[1]
const lang3 = langContainer.children[2]
const lang4 = langContainer.children[4]

const gtTlSugg = document.querySelector('#gt-tl-sugg')
const lang5 = gtTlSugg.children[0].children[0]
const lang6 = gtTlSugg.children[0].children[1]
const lang7 = gtTlSugg.children[0].children[2]

function triggerMouseEvent (node, eventType) {
  const clickEvent = document.createEvent ('MouseEvents')
  clickEvent.initEvent (eventType, true, true)
  node.dispatchEvent (clickEvent)
}

function emulateMouseClick(node) {
  triggerMouseEvent(node, 'mouseover')
  triggerMouseEvent(node, 'mousedown')
  triggerMouseEvent(node, 'mouseup')
  triggerMouseEvent(node, 'click')
}

document.addEventListener('keydown', (e) => {
  const key = e.code.slice(-1).toLowerCase()

  if (e.altKey && key === keyMap['srcListen']) {
    e.preventDefault()
    emulateMouseClick(srcListen)
  } else if (e.altKey && key === keyMap['srcClear']) {
    e.preventDefault()
    emulateMouseClick(srcClear)
  } else if (e.altKey && key === keyMap['langSwap']) {
    e.preventDefault()
    emulateMouseClick(langSwap)
  } else if (e.altKey && key === keyMap['destCopy']) {
    e.preventDefault()
    emulateMouseClick(destCopy)
  } else if (e.altKey && key === keyMap['destListen']) {
    e.preventDefault()
    emulateMouseClick(destListen)
  } else if (e.altKey && key === keyMap['srcArea']) {
    e.preventDefault()
    emulateMouseClick(source)
    source.focus()
  } else if (e.altKey && key === keyMap['phraseBook']) {
    e.preventDefault()
    emulateMouseClick(phraseBook)
  } else if (e.altKey && key === keyMap['srcLangMenu']) {
    e.preventDefault()
    emulateMouseClick(srcLangMenu)
  } else if (e.altKey && key === keyMap['destLangMenu']) {
    e.preventDefault()
    emulateMouseClick(destLangMenu)
  } else if (e.altKey && key === keyMap['lang1']) {
    e.preventDefault()
    emulateMouseClick(lang1)
  } else if (e.altKey && key === keyMap['lang2']) {
    e.preventDefault()
    emulateMouseClick(lang2)
  } else if (e.altKey && key === keyMap['lang3']) {
    e.preventDefault()
    emulateMouseClick(lang3)
  } else if (e.altKey && key === keyMap['lang4']) {
    e.preventDefault()
    emulateMouseClick(lang4)
  } else if (e.altKey && key === keyMap['lang5']) {
    e.preventDefault()
    emulateMouseClick(lang5)
  } else if (e.altKey && key === keyMap['lang6']) {
    e.preventDefault()
    emulateMouseClick(lang6)
  } else if (e.altKey && key === keyMap['lang7']) {
    e.preventDefault()
    emulateMouseClick(lang7)
  }
})

function applyCss() {
  const css = document.createElement('style')
  css.textContent = `
    #gt-src-wrap:after {
      color: #aaa;
      font-weight: normal;
      display: block;
      text-align: center;
      right: 3px;
      bottom: -2.5px;
      position: absolute;
      content: attr(data-hotkey) " to focus";
      top: auto;
    }
    #gt-src-listen:after {
      color: #aaa;
      font-weight: normal;
      position: relative;
      content: attr(data-hotkey);
      display: block;
      text-align: center;
      top: -12px;
    }
    #gt-swap:after {
      color: #aaa;
      font-weight: normal;
      position: relative;
      content: attr(data-hotkey);
      display: block;
      text-align: center;
      top: -8px;
    }
    #gt-clear:after {
      color: #aaa;
      font-weight: normal;
      position: relative;
      content: attr(data-hotkey);
      display: block;
      text-align: center;
      top: -3px;
    }
    #gt-res-copy:after{
      color: #aaa;
      font-weight: normal;
      position: relative;
      content: attr(data-hotkey);
      display: block;
      text-align: center;
      top: -10px;
    }
    #gt-res-listen:after {
      color: #aaa;
      font-weight: normal;
      position: relative;
      content: attr(data-hotkey);
      display: block;
      text-align: center;
      top: -11px;
    }
    #gt-langs .jfk-button:not([aria-hidden="true"]):not(#gt-swap):not(#gt-submit):after {
      color: #aaa;
      font-weight: normal;
      position: relative;
      content: attr(data-hotkey);
      counter-increment: langs;
    }
    #gt-sl-gms > div:nth-child(2):after,
    #gt-tl-gms > div:nth-child(2):after {
      color: #aaa;
      font-weight: normal;
      position: relative;
      content: attr(data-hotkey);
      display: block;
      text-align: center;
      left: -7px;
      top: -5px; 
    }
    #gt-pb-star .goog-toolbar-button:after {
      color: #aaa;
      font-weight: normal;
      position: relative;
      content: attr(data-hotkey);
      display: block;
      text-align: center;
      top: -10px;
    } 
  `
  document.head.appendChild(css)
}

const nodeList = [
  srcListen,
  srcClear,
  destCopy,
  destListen,
  langSwap,
  srcArea,
  lang1,
  lang2,
  lang3,
  lang4,
  lang5,
  lang6,
  lang7,
  srcLangMenu,
  destLangMenu,
  phraseBook
]

let keyMap = {
  'srcListen': 'k',
  'srcClear': 'd',
  'destCopy': 'c',
  'destListen': 'l',
  'langSwap': '0',
  'srcArea': 'f',
  'lang1': '1',
  'lang2': '2',
  'lang3': '3',
  'lang4': '4',
  'lang5': '5',
  'lang6': '6',
  'lang7': '7',
  'srcLangMenu': '8',
  'destLangMenu': '9',
  'phraseBook': 'p'
}

function mapping(nodeList, keyMap) {
  Object.values(keyMap).map((val, index) => {
    nodeList[index].setAttribute('data-hotkey', `(${val})`)
  })
}

browser.storage.local.get()
  .then(res => {
    if (res.map) {
      keyMap = res.map
      mapping(nodeList, keyMap)
    } else {
      mapping(nodeList, keyMap)
    }
  })
  .catch(err => console.log(err))

document.addEventListener('DOMContentLoaded', applyCss())
