const srcListen = document.querySelector('#src-listen')
const srcClear = document.querySelector('#src-clear')
const destCopy = document.querySelector('#dest-copy')
const destListen = document.querySelector('#dest-listen')
const langSwap = document.querySelector('#lang-swap')
const srcArea = document.querySelector('#src-area')
const phraseBook = document.querySelector('#dest-phrase')

const lang1 = document.querySelector('#lang1')
const lang2 = document.querySelector('#lang2')
const lang3 = document.querySelector('#lang3')
const lang4 = document.querySelector('#lang4')
const lang5 = document.querySelector('#lang5')
const lang6 = document.querySelector('#lang6')
const lang7 = document.querySelector('#lang7')

const srcLangMenu = document.querySelector('#src-lang-menu')
const destLangMenu = document.querySelector('#dest-lang-menu')

const btn = document.querySelector('button')
btn.addEventListener('click', () => {
  browser.storage.local.set({
    map: {
      'srcListen': srcListen.value,
      'srcClear': srcClear.value,
      'destCopy': destCopy.value,
      'destListen': destListen.value,
      'langSwap': langSwap.value,
      'srcArea': srcArea.value,
      'lang1': lang1.value,
      'lang2': lang2.value,
      'lang3': lang3.value,
      'lang4': lang4.value,
      'lang5': lang5.value,
      'lang6': lang6.value,
      'lang7': lang7.value,
      'srcLangMenu': srcLangMenu.value,
      'destLangMenu': destLangMenu.value,
      'phraseBook': phraseBook.value
    }
  }).then(() => {
    let succ = document.querySelector('#succ')
    succ.innerText = 'New hotkeys was saved.'
    setTimeout(() => {
      succ.innerText = ''
    }, 2000)
  })
})

const nodeList = [
  srcListen,
  srcClear,
  destCopy,
  destListen,
  langSwap,
  srcArea,
  lang1,
  lang2,
  lang3,
  lang4,
  lang5,
  lang6,
  lang7,
  srcLangMenu,
  destLangMenu,
  phraseBook
]

let keyMap = {
  'srcListen': 'k',
  'srcClear': 'd',
  'destCopy': 'c',
  'destListen': 'l',
  'langSwap': '0',
  'srcArea': 'f',
  'lang1': '1',
  'lang2': '2',
  'lang3': '3',
  'lang4': '4',
  'lang5': '5',
  'lang6': '6',
  'lang7': '7',
  'srcLangMenu': '8',
  'destLangMenu': '9',
  'phraseBook': 'p'
}

function mapping(nodeList, keyMap) {
  Object.values(keyMap).map((val, index) => nodeList[index].value = val)
}

browser.storage.local.get()
  .then(res => {
    if (res.map) {
      keyMap = res.map
      mapping(nodeList, keyMap)
    } else {
      mapping(nodeList, keyMap)
    }
  })
